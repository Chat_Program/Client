﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Net.Sockets;
using System.Collections.ObjectModel;

namespace Chat_program
{
    public partial class MainWindow : Window
    {
        
        public static int port;
        public static string serverIP; 
        public Client client;
        

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            if (ChatBox.Text != "")
            {
                if (client.Send(ChatBox.Text))
                {
                    client.PrintMessage(ChatBox.Text, client.name);
                    ChatBox.Text = "";
                }
            }
            
            
        }

        private void Connect(object sender, RoutedEventArgs e)
        {
            if (NameBox.Text != "")
            {
                string name = NameBox.Text;
                client = new Client
                {
                    itemsControl = viewChat,
                    scrollViewer = scrlViewer,
                    port = Int32.Parse(PortBox.Text),
                    ip = IPbox.Text,
                    name = name
                };
                if (client.Connect())
                {
                    ConnectBtn.IsEnabled = false;
                    ConnectBtn.Visibility = Visibility.Hidden;
                    NameBox.IsEnabled = false;
                    NameBox.Visibility = Visibility.Hidden;
                    PopUpMenu.Visibility = Visibility.Hidden;
                    
                    
                    client.Send(name);

                    ChatBox.IsEnabled = true;
                    SendBtn.IsEnabled = true;
                    ChatBox.Focus();
                    this.Title = "Chat - Connected";

                }
            }
        }

        private void ChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                Button_Click(sender, e);
            }
        }

        private void NameBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                Connect(sender, e);
            }
            NamePlaceholder.Visibility = Visibility.Hidden;

        }

        private void NameBoxFocus(object sender, RoutedEventArgs e)
        {
            if (NameBox.Text == "")
            {
                NamePlaceholder.Visibility = Visibility.Visible;
            }
            else
            {
                NamePlaceholder.Visibility = Visibility.Hidden;
            }
        }
    }

    public class Client
    {
        public string ip;
        public int port;
        TcpClient server;
        Socket socket;
        public string name;
        public ItemsControl itemsControl;
        public ScrollViewer scrollViewer;
        public ObservableCollection<Message> messageList = new ObservableCollection<Message>();
        
        public bool Connect()
        {
            try
            {
                Console.WriteLine("Connecting...");
                server = new TcpClient(ip, port);
                socket = server.Client;
                Console.WriteLine("Connected");
                Task.Factory.StartNew(() =>
                {
                    this.Receive();
        
                });
                itemsControl.DataContext = messageList;
                return true;
            }catch(Exception e){
                Console.WriteLine(e.ToString());
                Console.WriteLine("Couldn't Connect");
                return false;
            }
        }

        public bool Send(string message)
        {
            try
            {
                int byteCount = Encoding.Unicode.GetByteCount(message);
                byte[] sendData = new byte[byteCount];
                sendData = Encoding.Unicode.GetBytes(message);
                socket.Send(sendData);
                return true;
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Couldn't send");
                return false;
            }
        }

        public void Receive()
        {
            while (true)

                try
                {
                    byte[] receiveBuffer = new byte[1024];
                    Console.WriteLine("Receiving...");
                    int bytesRec = socket.Receive(receiveBuffer);
                    Array.Resize(ref receiveBuffer, bytesRec);
                    
                    string receivedMsg = Encoding.Unicode.GetString(receiveBuffer);
                    var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Message>(receivedMsg);
                    Console.WriteLine(receivedMsg);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                      
                        this.PrintMessage(obj.content, obj.name);
                        
                    });
                }
                catch
                {
                    Console.WriteLine("Connection Ended");
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        this.PrintMessage("Server closed.", "Server");
                    });
                    break;
                }
            }

        public void PrintMessage(string message, string name)
        {
            if(name == "Server")
            {
                messageList.Add(new ServerMsg(message, name));
            }
            else if(name != this.name)
            {
                messageList.Add(new MsgReceived(message, name));
            }
            else
            {
                messageList.Add(new MsgSent(message, name));
            }
            scrollViewer.ScrollToEnd();

        }

 
}


    public class Message
    {
        public string content { get; set; }
        public string name { get; set; }
        public Message(string content, string name)
        {
            this.content = content;
            this.name = name;
        }
    }

    public class MsgSent : Message
    {
        public MsgSent(string content, string name) : base(content, name) { }
    }
    public class MsgReceived : Message
    {
        public MsgReceived(string content, string name) : base(content, name) { }
    }
    public class ServerMsg : Message
    {
        public ServerMsg(string content, string name) : base(content, name) { } 
    }
}